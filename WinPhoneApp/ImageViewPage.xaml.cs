﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace WinPhoneApp
{
    using System.Threading.Tasks;
    using Windows.System.Display;
    using FilesList = List<StorageFile>;

    public class FrameServer
    {
        private static Random msRnd = new Random();

        private FilesList mFiles;
        private int[] mIndexesPermutation;
        private int mPermutationNextIdx;

        /// <summary>
        /// Asynchronous loading of an image from file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private async Task<BitmapImage> LoadBitmapFromFileAsync(StorageFile file)
        {
            BitmapImage bitmap = null;
            using (var stream = await file.OpenAsync(FileAccessMode.Read))
            {
                bitmap = new BitmapImage();
                bitmap.SetSource(stream);
            }
            return bitmap;
        }

        /// <summary>
        /// Randomly reorder elements of an array
        /// </summary>
        public static void Shuffle<T>(T[] array)
        {
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = msRnd.Next(n + 1);
                T value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
        }
        /// <summary>
        /// Generate next index in a random order
        /// </summary>
        /// <returns></returns>
        private int GenerateNextIndex()
        {
            int lastIdx = mIndexesPermutation.Length - 1;
            if (mPermutationNextIdx > lastIdx)
            {
                int last = mIndexesPermutation[lastIdx];
                Shuffle(mIndexesPermutation);
                //make sure the next file will be not equal to the previous one
                if (last == mIndexesPermutation[0])
                {
                    mIndexesPermutation[0] = mIndexesPermutation[lastIdx];
                    mIndexesPermutation[lastIdx] = last;
                }
                mPermutationNextIdx = 0;
            }
            return mIndexesPermutation[mPermutationNextIdx++];
        }


        //--------------------------------------------------------
        /// <summary>
        /// Create frame server from a list of files
        /// </summary>
        /// <param name="files"></param>
        public FrameServer(FilesList files)
        {
            mFiles = files;
            mIndexesPermutation = new int[files.Count()];
            for (int i = 0; i < mIndexesPermutation.Length; ++i)
            {
                mIndexesPermutation[i] = i;
            }
            Shuffle(mIndexesPermutation);
            mPermutationNextIdx = 0;
        }

        /// <summary>
        /// Get next frame/picture from the list
        /// Order depends on settings
        /// </summary>
        /// <returns></returns>
        public BitmapImage GetNext()
        {
            return LoadBitmapFromFileAsync(mFiles.ElementAt(GenerateNextIndex())).Result;
        }
        /// <summary>
        /// Get next frame/picture from the list
        /// Order depends on settings
        /// Async version
        /// </summary>
        /// <returns></returns>
        public async Task<BitmapImage> GetNextAsync()
        {
            return await LoadBitmapFromFileAsync(mFiles.ElementAt(GenerateNextIndex()));
        }
    }



    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ImageViewPage : Page
    {
        /// <summary>
        /// Parameters on navigation to this Frame
        /// </summary>
        public struct Parameters
        {
            public FilesList filesToShow;
            public double switchInterval;
        }
        //---------------------------------------------------------------
        //Data

        FrameServer mFrameServer = null;

        private DispatcherTimer mTimer = null;
        private double mSwitchInterval = 5.0;

        private DisplayRequest mDisplayRequest = null;

        //----------------------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        public ImageViewPage()
        {
            this.InitializeComponent();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }
        /// <summary>
        /// Reset images switch timer and start it from the current moment
        /// </summary>
        private void ResetTimer()
        {
            if (null != mTimer)
            {
                mTimer.Stop();
            }
            
            mTimer = new DispatcherTimer();
            mTimer.Interval = TimeSpan.FromSeconds(mSwitchInterval);
            mTimer.Tick += new EventHandler<object>(OnTick);
            mTimer.Start();
        }

        /// <summary>
        /// Change current image to the next one 
        /// The next image is picked from the input images list in a random order
        /// </summary>
        private void SwitchImage()
        {
            var frame = mFrameServer.GetNext();
            if (null != frame)
            {
                image.Source = frame;
            }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is Parameters)
            {
                var parameters = (Parameters)e.Parameter;
                mSwitchInterval = parameters.switchInterval;
                mFrameServer = new FrameServer(parameters.filesToShow);

                SwitchImage();
                ResetTimer();
            }
            else
            {
                throw new Exception("ImageViewPage[OnNavigatedTo]: Wrong parameters type!");
            }
            //Keep the screen enabled all time during the showing
            mDisplayRequest = new DisplayRequest();
            mDisplayRequest.RequestActive();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            //Free all references so GB can delete everything
            if (null != mTimer)
            {
                mTimer.Stop();
            }
            mTimer = null;

            BitmapImage bitmapImage = image.Source as BitmapImage;
            bitmapImage.UriSource = null;
            image.Source = null;

            mFrameServer = null;

            //Display can turn off on idle again
            mDisplayRequest.RequestRelease();
            mDisplayRequest = null;
        }


        /// <summary>
        /// On back go to the main page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                e.Handled = true;
                Frame.GoBack();
            }
        }

        /// <summary>
        /// Switch image on the tap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SwitchImage();
            ResetTimer();
        }

        /// <summary>
        /// Switch image on the timer tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTick(object sender, object e)
        {
            SwitchImage();
        }

    }
}
