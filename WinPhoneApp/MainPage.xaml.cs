﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace WinPhoneApp
{
    /// <summary>
    /// Class for binding thumbnail bitmaps to ImageView on the UI grid
    /// </summary>
    class ThumbnailItem
    {
        public Task<WriteableBitmap> ThnumbnailTask { set; get;}
        public int FileIndex { set; get; }

        public ThumbnailItem(Task<WriteableBitmap> bitmapTask, int index)
        {
            ThnumbnailTask = bitmapTask;
            FileIndex = index;
        }
    }

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// Class for holding information about every image for preview
        /// </summary>
        private struct ImageInfo
        {
            public StorageFile File { get; set; }
            public Task<WriteableBitmap> ThumbnailFuture { get; set; }

            public ImageInfo(StorageFile file, Task<WriteableBitmap> thumbnailTask)
            {
                File = file;
                ThumbnailFuture = thumbnailTask;
            }

        }
        //--------------------------------------------------------------------
        //Data

        private List<ImageInfo> mOpenedImages = new List<ImageInfo>();

        //--------------------------------------------------------------------
        /// <summary>
        /// Page initialization
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Method for updating interactive elements on the UI after user actions
        /// </summary>
        private void UpdateUiElements()
        {
            //Load images button - always enabled

            //Start button - enabled when there are loaded images
            button_start.IsEnabled = (mOpenedImages.Count() > 0);

            //No images text label - disappear when images are loaded
            selection_text_block.Visibility = (mOpenedImages.Count() > 0) ? Visibility.Collapsed : Visibility.Visible;
        }

        /// <summary>
        /// Setup thumbnails grid images; Should be called after any change in the list of opened images
        /// </summary>
        private void GenerateThumbnailsGrid()
        {
            List<ThumbnailItem> items = new List<ThumbnailItem>();
            for (int i = 0; i < mOpenedImages.Count(); ++i)
            {
                items.Add(new ThumbnailItem(mOpenedImages[i].ThumbnailFuture, i));
            }
            thumbnails_gridview.ItemsSource = items;
        }

        /// <summary>
        /// Create a thumbnail image for an image file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private async Task<WriteableBitmap> MakeThumbnail(StorageFile file)
        {
            WriteableBitmap thumbnail = new WriteableBitmap(1, 1);
            var stream = await file.OpenAsync(FileAccessMode.Read);
            thumbnail.SetSource(stream);
            stream.Dispose();
            return thumbnail;
        }


        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.
            UpdateUiElements();

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        /// <summary>
        /// Handle start button click; Switch to preview Frame and show images
        /// </summary>
        /// <param name="sender">Sender reference</param>
        /// <param name="e">Click arguments</param>
        private void button_start_Click(object sender, RoutedEventArgs e)
        {
            List<StorageFile> files = new List<StorageFile>();
            foreach (var info in mOpenedImages)
            {
                files.Add(info.File);
            }

            ImageViewPage.Parameters parameters;
            parameters.filesToShow = files;
            parameters.switchInterval = time_interval_slider.Value;
            Frame.Navigate(typeof(ImageViewPage), parameters);
        }

        //http://windowsapptutorials.com/windows-phone/media/using-fileopenpicker-in-windows-phone-8-1-to-choose-picture-from-picture-gallery/
        /// <summary>
        /// Handle Load images button click
        /// Open images picker 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_load_Click(object sender, RoutedEventArgs e)
        {
            FileOpenPicker filePicker = new FileOpenPicker();
            filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            filePicker.ViewMode = PickerViewMode.Thumbnail;

            filePicker.FileTypeFilter.Clear();
            filePicker.FileTypeFilter.Add(".bmp");
            filePicker.FileTypeFilter.Add(".png");
            filePicker.FileTypeFilter.Add(".jpg");
            filePicker.FileTypeFilter.Add(".jpeg");
            filePicker.FileTypeFilter.Add(".gif");

            filePicker.PickMultipleFilesAndContinue();

            CoreApplication.GetCurrentView().Activated += onViewActivated;
        }
        /// <summary>
        /// Handle event of the images picker return
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="iargs"></param>
        private void onViewActivated(CoreApplicationView sender, IActivatedEventArgs iargs)
        {
            FileOpenPickerContinuationEventArgs args = iargs as FileOpenPickerContinuationEventArgs;
            if (args != null)
            {
                CoreApplication.GetCurrentView().Activated -= onViewActivated;
                mOpenedImages.Clear();
                foreach (StorageFile file in args.Files)
                {
                    mOpenedImages.Add(new ImageInfo(file, MakeThumbnail(file)));
                }
            }
            GenerateThumbnailsGrid();
            UpdateUiElements();
        }

        /// <summary>
        /// Handle thumbnail click
        /// Delete image which thumbnail was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private async void OnThumbnailClick(object sender, ItemClickEventArgs args)
        {
            if (args != null && args.ClickedItem != null)
            {
                //Delete clicked thumbnail
                var resources = new Windows.ApplicationModel.Resources.ResourceLoader();

                var messageDialog = new MessageDialog(resources.GetString("alert_delete_image"));
                messageDialog.Commands.Add(new UICommand(resources.GetString("common_yes"), new UICommandInvokedHandler( (command) =>
                {
                    var thumbnail = args.ClickedItem as ThumbnailItem;
                    mOpenedImages.RemoveAt(thumbnail.FileIndex);
                    GenerateThumbnailsGrid();
                    UpdateUiElements();
                } )));
                messageDialog.Commands.Add(new UICommand(resources.GetString("common_no"), new UICommandInvokedHandler((command) => { })));
                messageDialog.DefaultCommandIndex = 1;
                messageDialog.CancelCommandIndex = 1;

                await messageDialog.ShowAsync();
            }
        }
        /// <summary>
        /// Display the current slider value in the text field above the slider
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (null != e && null != update_time_textblock)
            {
                update_time_textblock.Text = e.NewValue.ToString();
            }
        }

    }
}
